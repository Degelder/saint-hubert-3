# Liste des fichier pour le scss

## Components

Les components sass sont les fichiers dans lesquels on regroupe le style correspondant à des éléments réutilisables. Par exemple des boutons ou une barre de menu.

## Functions

Les fonctions sass sont similaires aux « mixins » à la différence qu’elles ne retournent pas un ensemble de code mais une seule valeur. Vous pouvez déclarer une fonction avec le mot clé « @function » comme il suit :

## Mixins

Les « mixins » sont des morceaux de codes paramétrables et réutilisables. Elles se déclarent avec le mot clé « @mixin » et s’utilisent avec le mot clé « @include ».

## Partials

Les « partials » sont des fichiers sass correspondant à une page ou portion de page spécifique à notre projet. Par exemple, dans « _about.scss » vous pourrez écrire du style spécifique à votre page ou votre encart où vous vous présentez sur votre site web.

# Fichiers

### _base.scss

On va écrire dans ce fichier le style global à l’application concernant la police par défaut, une couleur de fond… On écrira également ici tout le style que l’on souhaite surcharger. Par exemple si vous souhaitez surcharger le style par défaut des balises de lien « a », c’est ici que vous pouvez le faire

### _utils.scss

Dans ce fichier nous pourrons créer des classes utiles, comme pour gérer des marges. Ici c’est un fichier ou l’on va lister des petits éléments de style pratique.

### _variables.scss

Sass nous permet d’écrire des variables, je vous le montrerai plus tard. Sachez, que nous rassemblerons toutes les variables spécifiques à notre application (celles qui nous permettent de configurer notre design). Par exemple nous y reporterons le code couleur de notre application, des mesures qui peuvent être utilisées pour le responsive, dans des media query par exemple, ainsi que les polices de styles. En factorisant toutes ces valeurs, si la couleur principale de votre charte graphique change vous n’aurez qu’une valeur à modifier. Cool, non ?!

### style.scss

Vous aurez constaté qu’il s’agit du seul fichier non préfixé d’un « underscore », « _ ». Sass nous permet d’importer des fichiers sass dans d’autres fichiers sass, c’est ce que l’on appelle des « partials » (ou sassception ^^ ). C’est grâce à ces « partials » que l’on peut organiser notre dossier sass/scss comme ci-dessus. Le « underscore » permet de préciser à sass que les fichiers préfixés, comme ceci : « _nomDuFichier.scss », ne doivent pas être compilés directement mais doivent être au préalable importés. Vous pouvez en déduire qu’ici, tous nos fichiers doivent être importés à l’exception du « styles.scss », qui comportera tous notre style et qui sera compilé pour générer le css.

# _nomDuFichier.scss ?

Le « underscore » permet de préciser à sass que les fichiers préfixés, comme ceci : « _nomDuFichier.scss », ne doivent pas être compilés directement mais doivent être au préalable importés.

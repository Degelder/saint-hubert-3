<?php

namespace App\Repository;

use App\Entity\Courses;
use App\Entity\Taxis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Taxis|null find($id, $lockMode = null, $lockVersion = null)
 * @method Taxis|null findOneBy(array $criteria, array $orderBy = null)
 * @method Taxis[]    findAll()
 * @method Taxis[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaxisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Taxis::class);
    }

    // /**
    //  * @return Taxis[] Returns an array of Taxis objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Taxis
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function InfoChauffeur($chauffeurId){
       
       /* $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT v.immat as VehiculeId , t.id as TaxiId , s.id as SecteurId , ta.id as TarifId , t.nom , t.prenom , t.portable  , v.marque , v.model , v.nbrplace , v.energie , s.libelle , ta.prixttckm from App\Entity\Taxis t 
            join App\Entity\Vehicules v 
            join App\Entity\Tarifs ta 
            join App\Entity\Secteurs s 
            where t.id = :chauffeurId'
        )->setParameter('chauffeurId', $chauffeurId);

        // returns an array of Product objects
        return $query->getResult();
*/
        //-------------------------------------------------

        $query = $this->createQueryBuilder('t')
            ->select('v.immat as VehiculeId , t.id as TaxiId , s.id as SecteurId , ta.id as TarifId , t.nom , t.prenom , t.portable  , v.marque , v.model , v.nbrplace , v.energie , s.libelle , ta.prixttckm')
            ->join('t.fksecteur', 's')
            ->join('t.fkvehicule', 'v')
            ->join('t.fktarif', 'ta')
            ->where('t.id like :chauffeurId')
            ->setParameter('chauffeurId', $chauffeurId)
        ->getQuery()
        ->execute();

        return $query;
    }

    public function listeChauffeur($nbrePlace,$tarifMax,$secteur, $distance){
        
        $entityManager = $this->getEntityManager();

     /*   $query = $entityManager->createQuery(
            'SELECT  t.nom , t.prenom , v.marque , v.model , ta.prixttckm from App\Entity\Taxis t
            
            join App\Entity\Secteurs s 
            join App\Entity\Vehicules v 
            join App\Entity\Tarifs ta 
            where ta.prixttckm > 0 and ta.prixttckm < 5
            and s.libelle like \'valencienne\'
            and v.nbrplace >= 2'
        );
*/
        $query = $this->createQueryBuilder('t')
            ->join('t.fksecteur', 's')
            ->join('t.fkvehicule', 'v')
            ->join('t.fktarif', 'ta')
            ->where('s.libelle like :secteur')
            ->andWhere('ta.prixttckm * :distance < :max')
            ->andWhere(' v.nbrplace >= :nb')
            ->setParameter('secteur', $secteur)
            ->setParameter('distance', $distance)
            ->setParameter('max', $tarifMax)
            ->setParameter('nb', $nbrePlace)
        ->getQuery()
        ->execute();

     /*   ->setParameter('tarifMax', $tarifMax)
        ->setParameter('secteur', $secteur)
        ->setParameter('nbrePlace', $nbrePlace);*/
        

        // returns an array of Product objects
        return $query;
    }




}

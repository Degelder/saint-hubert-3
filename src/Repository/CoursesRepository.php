<?php

namespace App\Repository;

use App\Entity\Courses;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Courses|null find($id, $lockMode = null, $lockVersion = null)
 * @method Courses|null findOneBy(array $criteria, array $orderBy = null)
 * @method Courses[]    findAll()
 * @method Courses[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoursesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Courses::class);
    }

    // /**
    //  * @return Courses[] Returns an array of Courses objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Courses
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function ListeDemande($chauffeurId)
    {
        $query = $this->createQueryBuilder('c')
            ->select('c.id, cl.id as clientId ,c.date , c.heure , c.placenecessaire , c.depart , c.destination , cl.nom, cl.prenom , ta.prixttckm,c.distance')
            ->join('c.fksecteur', 's')
            ->join('c.fktaxi', 't')
            ->join('c.fktarif', 'ta')
            ->join('c.fkclient', 'cl')
            ->where('t.id like :chauffeurId')
            ->andWhere('c.valide = 0')
            ->andWhere('c.date >= current_date()')
            ->setParameter('chauffeurId', $chauffeurId)
            ->getQuery()
            ->execute();
        return $query;
    }

    public function ListeEnCour($chauffeurId)
    {
        $query = $this->createQueryBuilder('c')
            ->select('c.date , c.heure , c.placenecessaire , c.depart , c.destination ,c.distance, cl.nom, cl.prenom , ta.prixttckm')
            ->join('c.fksecteur', 's')
            ->join('c.fktaxi', 't')
            ->join('c.fktarif', 'ta')
            ->join('c.fkclient', 'cl')
            ->where('t.id like :chauffeurId')
            ->andWhere('c.valide = 1')
            ->andWhere('c.date >= current_date()')
            ->setParameter('chauffeurId', $chauffeurId)
            ->getQuery()
            ->execute();
        return $query;
    }

    public function ListeEffectuer($chauffeurId)
    {
        $query = $this->createQueryBuilder('c')
            ->select('c.date , c.heure , c.placenecessaire , c.depart , c.destination , cl.nom, cl.prenom , ta.prixttckm,c.distance')
            ->join('c.fksecteur', 's')
            ->join('c.fktaxi', 't')
            ->join('c.fktarif', 'ta')
            ->join('c.fkclient', 'cl')
            ->where('t.id like :chauffeurId')
            ->andWhere('c.valide = 1')
            ->andWhere('c.date < current_date()')
            ->setParameter('chauffeurId', $chauffeurId)
            ->getQuery()
            ->execute();
        return $query;
    }




    public function getCourses($ClientId)
    {

        /*    ->join('c.fkclient', 's')
            ->where('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
*/
        /*   ->setParameter('tarifMax', $tarifMax)
           ->setParameter('secteur', $secteur)
           ->setParameter('nbrePlace', $nbrePlace);*/
        $query = $this->createQueryBuilder('c')
            ->select('c.date , c.heure , c.placenecessaire ,c.id, c.note, c.distance,  c.depart , c.destination , cl.nom, cl.prenom, ta.prixttckm')
            ->join('c.fktarif', 'ta')
            ->join('c.fkclient', 'cl')
            ->where('cl.id like :ClientId')
            ->andWhere('c.valide = 1')
            ->setParameter('ClientId', $ClientId)
            ->orderBy('c.date', 'DESC')
            ->getQuery()
            ->execute();
        return $query;


    }


    public function getEvalCourse($ClientId, $idCourse)
    {

        /*    ->join('c.fkclient', 's')
            ->where('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
*/
        /*   ->setParameter('tarifMax', $tarifMax)
           ->setParameter('secteur', $secteur)
           ->setParameter('nbrePlace', $nbrePlace);*/
        $query = $this->createQueryBuilder('c')
            ->select('c.date ,c.id, c.heure , c.placenecessaire ,c.note, c.distance,  c.depart , c.destination, cl.nom, cl.prenom, ta.prixttckm')
            ->join('c.fkclient', 'cl')
            ->join('c.fktarif', 'ta')
            ->where('cl.id like :ClientId')
            ->andWhere('c.valide = 1')
            ->andWhere('c.id like :CourseId')
            ->setParameter('ClientId', $ClientId)
            ->setParameter('CourseId', $idCourse)
            ->getQuery()
            ->execute();
        return $query;


    }


    public function courseMail($courseId)
    {

        /*    ->join('c.fkclient', 's')
            ->where('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
*/
        /*   ->setParameter('tarifMax', $tarifMax)
           ->setParameter('secteur', $secteur)
           ->setParameter('nbrePlace', $nbrePlace);*/
        $query = $this->createQueryBuilder('c')
            ->select('c.date , c.heure , c.placenecessaire , c.depart , c.destination , c.distance , cl.nom, cl.prenom, ta.prixttckm , s.libelle')
            ->join('c.fktarif', 'ta')
            ->join('c.fkclient', 'cl')
            ->join('c.fksecteur', 's')
            ->where('c.id like :courseId')
            ->setParameter('courseId', $courseId)
            ->getQuery()
            ->execute();
        return $query;


    }
}

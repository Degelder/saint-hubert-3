<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/acceuil", name="acceuil")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/mailtest", name="mailtest")
     */
    public function mailtest()
    {
        $mail=[
            "validation"=>1,
            "date"=>"test",
            "heure"=>"test2",
            "placenecessaire"=>4,
            "depart"=>"denain",
            "destination"=>"denain",
            "secteur"=>"valenciennes",
            "tarif"=>32.5
        ];

        return $this->render('mail.html.twig', [
            'mail' => $mail,
        ]);
    }
}

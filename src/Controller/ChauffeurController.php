<?php

namespace App\Controller;

use App\Entity\Tarifs;
use App\Entity\Vehicules;
use App\Entity\Secteurs;
use App\Entity\Taxis;
use App\Entity\Courses;
use App\Entity\Utilisateur;
use App\Entity\Clients;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TelType;

class ChauffeurController extends AbstractController
{
    /**
     * @Route("/chauffeur", name="acceuil_chauffeur")
     */
    public function index()
    {
        
        $chauffeur = $this->getUser();
        $idChauffeur = $chauffeur->getFkTaxis()->getId();

        //recuperation des liste
        $listeDemande = $this->getDoctrine()->getRepository(Courses::class)->ListeDemande($idChauffeur);
        $listeEnCour = $this->getDoctrine()->getRepository(Courses::class)->ListeEnCour($idChauffeur);
        $listeEffectuer = $this->getDoctrine()->getRepository(Courses::class)->ListeEffectuer($idChauffeur);
                
        
        return $this->render('chauffeur/historique.html.twig', [
            'listeDemande' => $listeDemande,
            'listeEnCour' => $listeEnCour ,
            'listeEffectuer' => $listeEffectuer
        ]);
    }

    /**
     * @Route("/chauffeur/profil", name="profil_chauffeur")
     */
    public function profil(Request $request)
    {
        $chauffeur = $this->getUser();
        $idChauffeur = $chauffeur->getFkTaxis()->getId();
              

        //recuperation des informations
        $liste = $this->getDoctrine()->getRepository(Taxis::class)->InfoChauffeur($chauffeur->getFkTaxis()->getId());
             
        foreach($liste as $personne)
        {
            
            $info=["nom"=>$personne['nom'],
                   "prenom"=>$personne['prenom']
                ];
          
                
            

            //formulaire de modification du profil
            $form = $this->createFormBuilder()
                    ->add('numTel',TelType::class,['data' => $personne['portable'] ,'label'=>'numero de telephone:'])
                    ->add('immat',TextType::class,['data' => $personne['VehiculeId'],'label'=>'immatriculation:'])
                    ->add('modele',TextType::class,['data' => $personne['marque'],'label'=>'modele:'])
                    ->add('marque',TextType::class,['data' => $personne['model'],'label'=>'marque:'])
                    ->add('nbPlace',IntegerType::class,['data' => $personne['nbrplace'],'label'=>'nombre de places:'])
                    ->add('energie',TextType::class,['data' => $personne['energie'],'label'=>'energie:'])
                    ->add('tarifKm',NumberType::class,['data' => $personne['prixttckm'],'label'=>'tarifs au Km:'])
                    ->add('secteur',EntityType::class,[
                        'class'=> Secteurs::class ,
                        'data' => $personne['libelle'],
                        'label'=>'secteur:'])
                    ->add('save',SubmitType::class)
                    ->getForm();
        
            $form->handleRequest($request);

            //validation du formulaire
            if($form->isSubmitted() && $form->isValid())
                {
                     
                $data = $form->getData();
                var_dump($data['secteur']->getId());
                $entityManager = $this->getDoctrine()->getManager();
    
                $tabVehicules =$entityManager->getRepository(Vehicules::class)->findByImmat($personne['VehiculeId']);
                $tabTaxis = $entityManager->getRepository(Taxis::class)->findById($personne['TaxiId']);
                $tabTarifs =$entityManager->getRepository(Tarifs::class)->findById($personne['SecteurId']); 
    
                
                //modification dans la table vehicules
                if($personne['VehiculeId'] != $data['immat'])
                {
                    $vehicule = new Vehicules();
                    $vehicule->setImmat($data['immat']) 
                             ->setMarque($data['marque'])
                             ->setModel($data['modele'])
                             ->setNbrplace($data['nbPlace'])
                             ->setEnergie($data['energie']);
                $entityManager->persist($vehicule);
    
                $tabTaxis[0]->setFkvehicule( $vehicule);
                }
                else
                {
                    $tabVehicules[0]->setMarque($data['marque'])
                                    ->setModel($data['modele'])
                                    ->setNbrplace($data['nbPlace'])
                                    ->setEnergie($data['energie']);
                    $entityManager->persist($tabVehicules[0]);
                }
    
                
                //modification dans la table tarifs
                if($personne['prixttckm'] != $data['tarifKm'])
                {
                    $tarif = new Tarifs();
                    $tarif->setPrixttckm($data['tarifKm']);
                    $entityManager->persist($tarif);
    
    
    
                    $tabTaxis[0]->setFktarif($tarif);
                }
                
                
                //modification dans la table taxis
                
                $tabTaxis[0]->setPortable($data['numTel'])
                             ->setFksecteur($data['secteur']);
                $entityManager->persist($tabTaxis[0]);
                
                $entityManager->flush();
               
                //redirection
                return $this->redirectToRoute('profil_chauffeur');
                }
           
        }

        return $this->render('chauffeur/profil.html.twig', [
            'form' => $form->createView(),
            'personne' =>$info
        ]);
    }

    /**
     * @Route("/chauffeur/validation/{id}/{valide}/{clientId}", name="reponse_chauffeur")
     */
    public function validation($id , $valide, $clientId)
    {
        $entityManager = $this->getDoctrine()->getManager();
        
        //recup course
        $course= $entityManager->getRepository(Courses::class)->courseMail($id);
        $courseValide = $entityManager->getRepository(Courses::class)->findById($id);

        foreach($course as $data)
        {
            $mail=[
                "validation"=>$valide,
                "date"=>$data['date'],
                "heure"=>$data['heure'],
                "placenecessaire"=>$data['placenecessaire'],
                "depart"=>$data['depart'],
                "destination"=>$data['destination'],
                "secteur"=>$data['libelle'],
                "tarif"=>$data['distance']*$data['prixttckm']
            ];
        }
        //envoie des donnees sur le twig 
       

        //recup mail client
        $client= $entityManager->getRepository(Clients::class)->findById($clientId);
        $mailClient =  $client[0]->getMail();
       
        if($valide == 1)
        {
            $this->mail("acceptation de votre course",$mail,$mailClient);
            $courseValide[0]->setValide(1);
            $entityManager->persist($courseValide[0]);
        }
        else if ($valide == 0)
        {
            $this->mail("refus de votre course",$mail,$mailClient);
            $entityManager->remove($courseValide[0]);
        }
        $entityManager->flush();

        return $this->redirectToRoute('acceuil_chauffeur');
    }
    
    public function mail($contenuMail,$mail,$mailPersonne)
    {

 
    $transport = (new \Swift_SmtpTransport('smtp.gmail.com',465 ,'ssl'))
    ->setUsername('sainthubertvalarep@gmail.com')
    ->setPassword('sthu.8051S')
    ->setStreamOptions([
        'ssl' => ['allow_self_signed' => true, 'verify_peer' => false, 'verify_peer_name' => false]
    ]);
    // Create the Mailer using your created Transport
    $mailer = new \Swift_Mailer($transport);
    // Create a message
    $message = (new \Swift_Message($contenuMail))
    ->setFrom('sainthubertvalarep@gmail.com')
    ->setTo($mailPersonne)
    ->setBody($this->render(
        "mail.html.twig",[
        "mail"=>$mail]
    ),
    'text/html'
    );
  /*  ->setBody(
        $this->renderView(
            // templates/emails/registration.html.twig
            'emails/registration.html.twig',
            ['name' => $name]
        )
        , 'text/html')*/

    ;
    // Send the message
    $result = $mailer->send($message);

    return $this->render('admin/index.html.twig', [
        'controller_name' => 'AdminController',
    ]);
    }

    function getDistance($pointFirst, $pointSecond) {
        $url =  'https://maps.google.com/maps/api/directions/xml?language=fr&origin=' . urlencode($pointFirst) . '&destination=' . urlencode($pointSecond) . '&sensor=false&key=AIzaSyAK6wzXGoVwLdBN3Om39q-wztCGhvGmEk0&units=metric';


        $api = simplexml_load_string(file_get_contents(htmlspecialchars_decode ($url)));

        return $api->route->leg->distance->value/1000 ;
    }

}

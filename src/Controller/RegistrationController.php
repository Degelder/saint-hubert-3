<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Entity\Clients;
use App\Entity\Taxis;
use App\Entity\Secteurs;
use App\Entity\Tarifs;
use App\Entity\Vehicules;
use App\Form\RegistrationFormType;
use App\Security\LoginFormAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;


class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator): Response
    {
        $user = [
            "login"=>null,
            "password"=>null,
            "nom"=>null,
            "prenom"=>null,
            "portable"=>null,
            "type"=>null,
            "mail"=>null
        ];
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $user= $form->getData();
            if( $user["type"]=="client"){

                $client=new clients();
                $client
                ->setNom($user["nom"])
                ->setPrenom($user["prenom"])
                ->setPortable($user["portable"])
                ->setMail($user["mail"])
                ;

                $entityManager->persist($client);

                $userBase=new Utilisateur();
                $userBase
                ->setUsername($user["login"])
                ->setRoles(array("ROLE_CLIENT"))
                // encode the plain password
                ->setPassword(
                    $passwordEncoder->encodePassword(
                        $userBase,
                        $user["password"]
                    )
                )
                ->setFkClients($client)
                ;
                $entityManager->persist($userBase);

            }else{
                $chauffeur=new taxis();
                $chauffeur
                ->setNom($user["nom"])
                ->setPrenom($user["prenom"])
                ->setPortable($user["portable"]);

                $entityManager= $this->getDoctrine()->getManager();
                $repository = $entityManager ->getRepository(Vehicules::class);
                $vehicule = $repository ->find(1);

                $repository = $entityManager ->getRepository(Tarifs::class);
                $tarif = $repository ->find(1);

                $repository = $entityManager ->getRepository(Secteurs::class);
                $secteur = $repository ->find(1);


                $chauffeur
                ->setFkvehicule($vehicule)
                ->setFktarif($tarif)
                ->setFksecteur($secteur)
                ;

                $entityManager->persist($chauffeur);

                $userBase=new Utilisateur();
                $userBase
                ->setUsername($user["login"])
                ->setRoles(array("ROLE_CHAUFFEUR"))
                // encode the plain password
                ->setPassword(
                    $passwordEncoder->encodePassword(
                        $userBase,
                        $user["password"]
                    )
                )
                ->setFkTaxis($chauffeur)
                ;
                $entityManager->persist($userBase);
            }
            

            $entityManager->flush();

            // do anything else you need here, like send an email

            return $guardHandler->authenticateUserAndHandleSuccess(
                $userBase,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
            
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}

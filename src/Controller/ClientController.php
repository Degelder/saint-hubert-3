<?php

namespace App\Controller;

use App\Entity\Clients;
use App\Entity\Courses;
use App\Entity\Secteurs;
use App\Entity\Tarifs;
use App\Entity\Utilisateur;
use App\Repository\CoursesRepository;
use Doctrine\DBAL\Types\FloatType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use App\Entity\Taxis;
use App\Repository\TaxisRepository;
use Symfony\Component\Validator\Constraints\Date;
use App\Controller\apiController;
use function Sodium\add;


class ClientController extends AbstractController
{
    /**
     * @Route("/client/recherche", name="client_recherche")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function recherche(Request $request)
    {

        $form = ["date" => null,
            "heure" => null,
            "depart" => null,
            "destination" => null,
            "libelle" => null,
            "nb_place" => 3,
            "tarifMax" => null,
        ];

        $form = $this->createFormBuilder()
            ->add('date', DateType::class, ['data' => $form['date']])
            ->add('heure', TimeType::class, ['data' => $form['heure']])
            ->add('depart', TextType::class, ['data' => $form['depart']])
            ->add('destination', TextType::class, ['data' => $form['destination']])
            ->add('secteur', EntityType::class, [
                'class' => Secteurs::class,
                'data' => $form['libelle']])
            ->add('nb_place', IntegerType::class, ['data' => $form['nb_place']])
            ->add('tarifMax', NumberType::class, ['data' => $form['tarifMax']])
            ->add('Rechercher', SubmitType::class)
            ->getForm();


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


            $data = $form->getData();
            $date = $data['date'];
            $dateString = $date->format('d-m-Y');
            $heure = $data['heure'];
            $heureString = $heure->format('H:i');


            return $this->redirectToRoute('client_recherche_validation', ['date' => $dateString,
                'heure' => $heureString, 'depart' => $data['depart'], 'destination' => $data['destination'],
                'nbPlace' => $data['nb_place'], 'tarifMax' => $data['tarifMax'], 'secteur' => $data['secteur']]);


        }

        return $this->render('client/recherche.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/client/recherche/{geo}", name="client_recherche_geo")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function recherche_geo(Request $request,$geo)
    {

        $form = ["date" => null,
            "heure" => null,
            "depart" => null,
            "destination" => null,
            "libelle" => null,
            "nb_place" => 3,
            "tarifMax" => null,
        ];
        $url = 'https://maps.google.com/geolocation/v1/geolocate?key=AIzaSyAK6wzXGoVwLdBN3Om39q-wztCGhvGmEk0';
        $api = file_get_contents(htmlspecialchars_decode($url));
        $geo=$api;

        $form = $this->createFormBuilder()
            ->add('date', DateType::class, ['data' => $form['date']])
            ->add('heure', TimeType::class, ['data' => $form['heure']])
            ->add('depart', TextType::class, ['data' => $geo])
            ->add('destination', TextType::class, ['data' => $form['destination']])
            ->add('secteur', EntityType::class, [
                'class' => Secteurs::class,
                'data' => $form['libelle']])
            ->add('nb_place', IntegerType::class, ['data' => $form['nb_place']])
            ->add('tarifMax', NumberType::class, ['data' => $form['tarifMax']])
            ->add('Rechercher', SubmitType::class)
            ->getForm();


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {


            $data = $form->getData();
            $date = $data['date'];
            $dateString = $date->format('d-m-Y');
            $heure = $data['heure'];
            $heureString = $heure->format('H:i');


            return $this->redirectToRoute('client_recherche_validation', ['date' => $dateString,
                'heure' => $heureString, 'depart' => $data['depart'], 'destination' => $data['destination'],
                'nbPlace' => $data['nb_place'], 'tarifMax' => $data['tarifMax'], 'secteur' => $data['secteur']]);


        }

        return $this->render('client/recherche.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/client/recherche/{date}/{heure}/{depart}/{destination}/{nbPlace}/{tarifMax}/{secteur}", name="client_recherche_validation")
     * @param Request $request
     * @param $date
     * @param $heure
     * @param $depart
     * @param $destination
     * @param $nbPlace
     * @param $secteur
     * @param $tarifMax
     * @param \Doctrine\Common\Persistence\ManagerRegistry $managerRegistry
     * @param $secteur
     * @return Response
     */
    public function rechercheTaxis(Request $request, $date, $heure, $depart, $destination, $nbPlace, $tarifMax, $secteur)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $distance = $this->getDistance($depart, $destination);
        $taxisRepo = $entityManager->getRepository(Taxis::class);
        $listeTaxis = $taxisRepo->listeChauffeur($nbPlace, $tarifMax, $secteur, $distance);


        return $this->render('client/recherche_validation.html.twig', ['date' => $date, 'heure' => $heure, 'listeTaxis' => $listeTaxis, 'depart' => $depart
            , 'destination' => $destination, 'nbplaces' => $nbPlace, 'tarifMax' => $tarifMax, 'secteur' => $secteur, 'distance' => $distance]);
    }


    /**
     * @Route("/client/addcourse/{date}/{heure}/{depart}/{destination}/{nbPlaces}/{taxis}/{distance}", name="add_course")
     * @param Request $request
     * @param $depart
     * @param $heure
     * @param $destination
     * @param $nbPlaces
     * @param $date
     * @param Taxis $taxis
     * @param $distance
     * @return RedirectResponse
     */
    public function addCourse(Request $request, $depart, $heure, $destination, $nbPlaces, $date, Taxis $taxis, $distance)
    {
        $distance = str_replace(',', '.', $distance);
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $client = $user->getFkClients();
        $tarif = $taxis->getFktarif();
        $secteur = $taxis->getFksecteur();

        $heure = \DateTime::createFromFormat('H:i', $heure);
        $date = \DateTime::createFromFormat('d-m-Y', $date);

        $course = new Courses();
        $course->setDate($date);
        $course->setHeure($heure);
        $course->setDepart($depart);
        $course->setDestination($destination);
        $course->setFkclient($client);
        $course->setFksecteur($secteur);
        $course->setFktarif($tarif);
        $course->setFktaxi($taxis);
        $course->setPlacenecessaire($nbPlaces);
        $course->setDate($date);
        $course->setDistance($distance);


        $em->persist($course);
        $em->flush();

        return $this->redirectToRoute("index_client");

    }

    /**
     * @Route("/client/index", name="index_client")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $user = $this->getUser();
        $client = $user->getFkClients();
        $idClient = $client->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Courses::class);
        $courses = $repository->getCourses($idClient);


        return $this->render('client/index.html.twig', ["user" => $client, "courses" => $courses]);

    }


    /**
     * @Route("/client/evaluation/{id<\d+>}", name="evaluation")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function evaluation(Request $request, $id)
    {
        $user = $this->getUser();
        $client = $user->getFkClients();
        $idClient = $client->getId();
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Courses::class);
        $course = $repository->getEvalCourse($idClient, $id);

        $form = $this->createFormBuilder()
            ->add('note', ChoiceType::class, ['choices' => [
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5,
            ],])
            ->add('Evaluer', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute('add_evaluation', ['course' => $course, 'note' => $data['note'], 'id' => $course[0]['id']]);
        }

        return $this->render('client/evaluation.html.twig', ["user" => $client, "course" => $course, 'form' => $form->createView()]);
    }

    /**
     * @Route("/client/addEval/{id<\d+>}/{note}", name="add_evaluation")
     * @param Request $request
     * @param $note
     * @param Courses $id
     * @return Response
     */
    public function addEvaluation(Request $request, $note, Courses $id)
    {
        $entityManager = $this->getDoctrine()->getManager();


        $id->setNote($note);
        $entityManager->persist($id);
        $entityManager->flush();
        $user = $this->getUser();
        $client = $user->getFkClients();
        $idClient = $client->getId();
        $repository = $entityManager->getRepository(Courses::class);
        $course = $repository->getEvalCourse($idClient, $id);
        return $this->redirectToRoute('index_client', ['user' => $client, 'courses' => $course]);
    }

    function getDistance($pointFirst, $pointSecond)
    {
        $url = 'https://maps.google.com/maps/api/directions/xml?language=fr&origin=' . urlencode($pointFirst) . '&destination=' . urlencode($pointSecond) . '&sensor=false&key=AIzaSyAK6wzXGoVwLdBN3Om39q-wztCGhvGmEk0&units=metric';


        $api = simplexml_load_string(file_get_contents(htmlspecialchars_decode($url)));

        return $api->route->leg->distance->value / 1000;
    }

}

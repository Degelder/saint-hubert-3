<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vehicules
 *
 * @ORM\Table(name="vehicules")
 * @ORM\Entity(repositoryClass="App\Repository\VehiculesRepository")
 */
class Vehicules
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="immat", type="string", length=45, nullable=true)
     */
     private $immat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="marque", type="string", length=45, nullable=true)
     */
    private $marque;

    /**
     * @var string|null
     *
     * @ORM\Column(name="model", type="string", length=45, nullable=true)
     */
    private $model;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nbrPlace", type="integer", nullable=true)
     */
    private $nbrplace;

    /**
     * @var string|null
     *
     * @ORM\Column(name="energie", type="string", length=45, nullable=true)
     */
    private $energie;

    public function getImmat(): ?string
    {
        return $this->immat;
    }

    public function setImmat(?string $immat): self
    {
        $this->immat = $immat;

        return $this;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(?string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(?string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getNbrplace(): ?int
    {
        return $this->nbrplace;
    }

    public function setNbrplace(?int $nbrplace): self
    {
        $this->nbrplace = $nbrplace;

        return $this;
    }

    public function getEnergie(): ?string
    {
        return $this->energie;
    }

    public function setEnergie(?string $energie): self
    {
        $this->energie = $energie;

        return $this;
    }


}

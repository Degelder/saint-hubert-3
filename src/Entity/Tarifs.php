<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tarifs
 *
 * @ORM\Table(name="tarifs")
 * @ORM\Entity(repositoryClass="App\Repository\TarifsRepository")
 */
class Tarifs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
   

    /**
     * @var float|null
     *
     * @ORM\Column(name="prixTtcKm", type="float", precision=10, scale=0, nullable=true)
     */
    private $prixttckm;

    public function getId(): ?int
    {
        return $this->id;
    }
   

    public function getPrixttckm(): ?float
    {
        return $this->prixttckm;
    }

    public function setPrixttckm(?float $prixttckm): self
    {
        $this->prixttckm = $prixttckm;

        return $this;
    }


}

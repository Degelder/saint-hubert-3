<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Taxis
 *
 * @ORM\Table(name="taxis", indexes={ @ORM\Index(name="secteurs_idx", columns={"fkSecteur"}), @ORM\Index(name="tarif_idx", columns={"fkTarif"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaxisRepository")
 */
class Taxis
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=45, nullable=true)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="prenom", type="string", length=45, nullable=true)
     */
    private $prenom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="portable", type="string", length=45, nullable=true)
     */
    private $portable;

    /**
     * @var \Secteurs
     *
     * @ORM\ManyToOne(targetEntity="Secteurs")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="fkSecteur", referencedColumnName="id")
     * })
     */
    private $fksecteur;

    /**
     * @var \Tarifs
     *
     * @ORM\ManyToOne(targetEntity="Tarifs")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="fkTarif", referencedColumnName="id")
     * })
     */
    private $fktarif ;

    /**
     * @var \Vehicules
     *
     * @ORM\ManyToOne(targetEntity="Vehicules")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="fkVehicule", referencedColumnName="id",nullable=true)
     * })
     */
    private $fkvehicule ;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $moyenne;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getPortable(): ?string
    {
        return $this->portable;
    }

    public function setPortable(?string $portable): self
    {
        $this->portable = $portable;

        return $this;
    }

    public function getFksecteur(): ?Secteurs
    {
        return $this->fksecteur;
    }

    public function setFksecteur(?Secteurs $fksecteur): self
    {
        $this->fksecteur = $fksecteur;

        return $this;
    }

    public function getFktarif(): ?Tarifs
    {
        return $this->fktarif;
    }

    public function setFktarif(?Tarifs $fktarif): self
    {
        $this->fktarif = $fktarif;

        return $this;
    }

    public function getFkvehicule(): ?Vehicules
    {
        return $this->fkvehicule;
    }

    public function setFkvehicule(?Vehicules $fkvehicule): self
    {
        $this->fkvehicule = $fkvehicule;

        return $this;
    }

    public function getMoyenne(): ?float
    {
        return $this->moyenne;
    }

    public function setMoyenne(?float $moyenne): self
    {
        $this->moyenne = $moyenne;

        return $this;
    }


}

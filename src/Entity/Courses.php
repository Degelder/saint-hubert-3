<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Courses
 *
 * @ORM\Table(name="courses", indexes={@ORM\Index(name="tarif_idx", columns={"fkTarif"}), @ORM\Index(name="taxi_idx", columns={"fkTaxi"}), @ORM\Index(name="client_idx", columns={"fkClient"}), @ORM\Index(name="secteur_idx", columns={"fkSecteur"})})
 * @ORM\Entity(repositoryClass="App\Repository\CoursesRepository")
 */
class Courses
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="heure", type="time", nullable=true)
     */
    private $heure;

    /**
     * @var int|null
     *
     * @ORM\Column(name="placeNecessaire", type="integer", nullable=true)
     */
    private $placenecessaire;

    /**
     * @var string|null
     *
     * @ORM\Column(name="depart", type="string", length=45, nullable=true)
     */
    private $depart;

    /**
     * @var string|null
     *
     * @ORM\Column(name="destination", type="string", length=45, nullable=true)
     */
    private $destination;

  

    /**
     * @var int|null
     *
     * @ORM\Column(name="note", type="integer", nullable=true)
     */
    private $note;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="valide", type="boolean", nullable=true)
     */
    private $valide = 0;

    /**
     * @var \Clients
     *
     * @ORM\ManyToOne(targetEntity="Clients")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fkClient", referencedColumnName="id")
     * })
     */
    private $fkclient;

    /**
     * @var \Secteurs
     *
     * @ORM\ManyToOne(targetEntity="Secteurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fkSecteur", referencedColumnName="id")
     * })
     */
    private $fksecteur;

    /**
     * @var \Tarifs
     *
     * @ORM\ManyToOne(targetEntity="Tarifs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fkTarif", referencedColumnName="id")
     * })
     */
    private $fktarif;

    /**
     * @var \Taxis
     *
     * @ORM\ManyToOne(targetEntity="Taxis")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fkTaxi", referencedColumnName="id")
     * })
     */
    private $fktaxi;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $distance;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getHeure(): ?\DateTimeInterface
    {
        return $this->heure;
    }

    public function setHeure(?\DateTimeInterface $heure): self
    {
        $this->heure = $heure;

        return $this;
    }

    public function getPlacenecessaire(): ?int
    {
        return $this->placenecessaire;
    }

    public function setPlacenecessaire(?int $placenecessaire): self
    {
        $this->placenecessaire = $placenecessaire;

        return $this;
    }

    public function getDepart(): ?string
    {
        return $this->depart;
    }

    public function setDepart(?string $depart): self
    {
        $this->depart = $depart;

        return $this;
    }

    public function getDestination(): ?string
    {
        return $this->destination;
    }

    public function setDestination(?string $destination): self
    {
        $this->destination = $destination;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(?int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(?bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

    public function getFkclient(): ?Clients
    {
        return $this->fkclient;
    }

    public function setFkclient(?Clients $fkclient): self
    {
        $this->fkclient = $fkclient;

        return $this;
    }

    public function getFksecteur(): \Secteurs
    {
        return $this->fksecteur;
    }

    public function setFksecteur(?Secteurs $fksecteur): self
    {
        $this->fksecteur = $fksecteur;

        return $this;
    }

    public function getFktarif(): \Tarifs
    {
        return $this->fktarif;
    }

    public function setFktarif(?Tarifs $fktarif): self
    {
        $this->fktarif = $fktarif;

        return $this;
    }

    public function getFktaxi(): \Taxis
    {
        return $this->fktaxi;
    }

    public function setFktaxi(?Taxis $fktaxi): self
    {
        $this->fktaxi = $fktaxi;

        return $this;
    }

    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(?float $distance): self
    {
        $this->distance = $distance;

        return $this;
    }
}

